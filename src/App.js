import { useEffect, useState } from "react";
import "./App.css";
import calculateTimeLeft from "./libs/calculateTimeLeft";
import randomColor from "./libs/randomColor";

function App() {
  const deadline = new Date("04/12/2021");
  const [backgroundColor, setBackgroundColor] = useState(randomColor().hex);
  // setDeadline for future modifications e.g. custom deadline setter
  const [timeLeft, setTimeLeft] = useState(calculateTimeLeft(deadline));

  function startInterval() {
    let intervalID;
    setInterval(() => {
      intervalID = setTimeLeft(calculateTimeLeft("04/12/2021"));
    }, 1000);
    return () => {
      clearInterval(intervalID);
    };
  }

  useEffect(() => {
    startInterval();
  }, []);
  useEffect(() => {
    setBackgroundColor(randomColor().hex);
    document.title = `
    ${timeLeft.days}d-${timeLeft.hours}h-${timeLeft.minutes}m-${timeLeft.seconds}s`;
  }, [timeLeft]);
  return (
    <div className="App">
      <header className="App-header">
        <h1
          style={{
            color: backgroundColor,
            padding: "1em",
            border: `3px dashed ${backgroundColor}`,
          }}
        >
          DEADLINE!
        </h1>
        <h2>
          <strong>{deadline.getDate()}</strong> -
          <strong> {deadline.getMonth() + 1}</strong> -
          <strong> {deadline.getFullYear()}</strong>
        </h2>
        <span>{timeLeft.days} dni</span>
        <span>{timeLeft.hours} godzin</span>
        <span>{timeLeft.minutes} minut</span>
        <span>{timeLeft.seconds} sekund</span>
      </header>
    </div>
  );
}

export default App;
