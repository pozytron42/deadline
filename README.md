# Deadline counter

[deadline.nertigo.com](https://thirsty-brown-c55664.netlify.app/)

As simple as that!
First release is a rapidly hacked cruel first version of counting down time in react.

## Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.
